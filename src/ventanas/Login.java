
package ventanas;

import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.sql.*;
import Coneccion.Coneccion;
import javax.swing.JOptionPane;

public class Login extends javax.swing.JFrame 
{   
   public static String user = "";
   public static String pass = "";  
   
   public  Login() 
   {
        initComponents();
  //-----------caracteristicas del login tamaño titulo localizacion movible
        setSize(400, 550);
        setResizable(false);
        setTitle("Acceso al sistema");
        setLocationRelativeTo(null);
            
  //----------Se pone un label wallpaper se le pone una imagen  y se saca de paquete con nombre y extencion 
        ImageIcon wallpaper = new ImageIcon("src/imagenes/wallpaperPrincipal.jpg");
     
  //----------Se nombra al objeto Icon y se pone las mismas medidas del label para que quede al mismo tamaño
        Icon icono = new ImageIcon(wallpaper.getImage().getScaledInstance(label_wallpaper.getWidth(),
                label_wallpaper.getHeight(), Image.SCALE_DEFAULT));
        
 //----------Se establece la imgen escogida al label 
        label_wallpaper.setIcon(icono);
        
 //-----------Para que se actualice en cada modificacion es el repaint      
        this.repaint();
                     
        ImageIcon wallpaper_logo = new ImageIcon("src/imagenes/descarga.jpg");
        Icon icono_logo = new ImageIcon(wallpaper_logo.getImage().getScaledInstance(label_logo.getWidth(),
          label_logo.getHeight(), Image.SCALE_DEFAULT));
          label_logo.setIcon(icono_logo);

        this.repaint();  //Esta linea de código es opcional a veces es necesaria para ue se noten los cambios y se siga viendo la imagen    
    }
    
       //--------Como cambiar el logo de java de la esquina ------------------
//-----------Te tienes que ir a propiedades del JFrame y escoger iconimage y decir que los vas a agarrar de value from existen component  lo escoges de ahi tambien
    @Override
    public Image getIconImage() 
    {
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("imagenes/descarga.jpg"));
        return retValue;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        label_logo = new javax.swing.JLabel();
        text_user = new javax.swing.JTextField();
        text_password = new javax.swing.JPasswordField();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        label_wallpaper = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());
        setIconImages(getIconImages());
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(label_logo, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 80, 250, 130));

        text_user.setBackground(new java.awt.Color(153, 153, 255));
        text_user.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        text_user.setForeground(new java.awt.Color(255, 255, 255));
        text_user.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(text_user, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 260, 160, 30));

        text_password.setBackground(new java.awt.Color(153, 153, 255));
        text_password.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        text_password.setForeground(new java.awt.Color(255, 255, 255));
        text_password.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        text_password.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                text_passwordActionPerformed(evt);
            }
        });
        getContentPane().add(text_password, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 310, 160, 30));

        jButton1.setBackground(new java.awt.Color(153, 153, 255));
        jButton1.setFont(new java.awt.Font("Arial Narrow", 1, 18)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("ACCEDER");
        jButton1.setBorder(null);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 370, 190, 50));

        jLabel1.setText("Creado por Estudiantes IMEC");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 500, -1, -1));
        getContentPane().add(label_wallpaper, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 520));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
      datosIngresar();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void text_passwordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_text_passwordActionPerformed
      datosIngresar();
    }//GEN-LAST:event_text_passwordActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel label_logo;
    private javax.swing.JLabel label_wallpaper;
    private javax.swing.JPasswordField text_password;
    private javax.swing.JTextField text_user;
    // End of variables declaration//GEN-END:variables

    public void datosIngresar()
    {
        user = text_user.getText().trim();
        pass = text_password.getText().trim();
        
        if(!user.equals("") || !pass.equals(""))
        {
            try
            {
                Connection cn = Coneccion.conectar();
                PreparedStatement pst = cn.prepareStatement(
                "select nivel, status from usuarios where username = '"+ user
                 +"' and password = '"+ pass + "'");
                ResultSet rs = pst.executeQuery();
                if(rs.next())
                {                 
                    String tipo_nivel = rs.getString("nivel");
                    String estatus = rs.getString("status");
                    
                    if(tipo_nivel.equalsIgnoreCase("Administrador") && estatus.equalsIgnoreCase("Activo"))
                    {
                        dispose();
                        new Visitas().setVisible(true);           
                    }
                    else if(tipo_nivel.equalsIgnoreCase("Capturista") && estatus.equalsIgnoreCase("Activo"))
                    {
                        dispose();
//                        new  Capturista().setVisible(true);
                    }
                    else if(tipo_nivel.equalsIgnoreCase("Tecnico") && estatus.equalsIgnoreCase("Activo"))
                    {
                        dispose();
//                        new Tecnico().setVisible(true);
                    }                 
                }
                else
                {
                     JOptionPane.showMessageDialog(null, "Datos de acceso incorectos");
                     text_user.setText("");
                     text_password.setText("");
                }              
            }
            catch(SQLException e)
            {
                System.err.println("Error en el boton Acceder "+e);
                JOptionPane.showMessageDialog(null, "¡¡Error al iniciar sesión, Contacta al administrador.");
            }
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Debes llenar todos los campos");
        }     
    }

}
