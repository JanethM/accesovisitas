/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventanas;

import Coneccion.Coneccion;
import java.awt.Image;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import static ventanas.Login.user;

/**
 *
 * @author Janeth
 */
public class Visitas extends javax.swing.JFrame {
  
    public Visitas() {
        initComponents();
        user = Login.user;
        setSize(708, 429);
        setResizable(false);
        setTitle("Administrador - Sesión de "+user);
        setLocationRelativeTo(null);
        
        
        //------ se crea un objeto para obtener la imagen del wallpaper
        ImageIcon wallpaper = new ImageIcon("src/imagenes/wallpaperPrincipal.jpg");

//----------Se nombra al objeto Icon y se pone las mismas medidas del label para que quede al mismo tamaño
        Icon icono = new ImageIcon(wallpaper.getImage().getScaledInstance(label_wallpaper.getWidth(),
        label_wallpaper.getHeight(), Image.SCALE_DEFAULT));
        label_wallpaper.setIcon(icono);
        this.repaint();
        
           try{
            Connection cn = Coneccion.conectar();
            PreparedStatement pst = cn.prepareStatement(
            "select nombre from usuarios where username = '"+ user +"'");
            ResultSet rs = pst.executeQuery();
            if(rs.next()){
                String nombre_usuario = rs.getString("nombre");
               label_nombre_usuario.setText("Bienvenido "+nombre_usuario);             
            }
        }
        catch(Exception e)
        {
           System.out.println("Error en consultar nombre de Usuario");
        }        
    }

       //--------Como cambiar el logo de java de la esquina 
    @Override
    public Image getIconImage() 
    {
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("imagenes/descarga.jpg"));
        return retValue;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        label_nombre_usuario = new javax.swing.JLabel();
        label_registro_visita = new javax.swing.JLabel();
        label_gestionar_visita = new javax.swing.JLabel();
        label_imprimir_visita = new javax.swing.JLabel();
        label_estudiantes = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        label_wallpaper = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        label_nombre_usuario.setBackground(new java.awt.Color(153, 153, 255));
        getContentPane().add(label_nombre_usuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 50, 260, 60));

        label_registro_visita.setFont(new java.awt.Font("Calisto MT", 1, 14)); // NOI18N
        label_registro_visita.setForeground(new java.awt.Color(0, 0, 102));
        label_registro_visita.setText("REGISTRAR VISITAS");
        getContentPane().add(label_registro_visita, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 270, -1, -1));

        label_gestionar_visita.setFont(new java.awt.Font("Calisto MT", 1, 14)); // NOI18N
        label_gestionar_visita.setForeground(new java.awt.Color(0, 0, 102));
        label_gestionar_visita.setText("GESTIONAR VISITA");
        getContentPane().add(label_gestionar_visita, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 270, -1, -1));

        label_imprimir_visita.setFont(new java.awt.Font("Calisto MT", 1, 14)); // NOI18N
        label_imprimir_visita.setForeground(new java.awt.Color(0, 0, 102));
        label_imprimir_visita.setText("IMPRIMIR VISITAS");
        getContentPane().add(label_imprimir_visita, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 270, -1, -1));

        label_estudiantes.setFont(new java.awt.Font("Calisto MT", 1, 14)); // NOI18N
        label_estudiantes.setForeground(new java.awt.Color(0, 0, 102));
        label_estudiantes.setText("Hecho por: ESTUDIANTES IMEC");
        getContentPane().add(label_estudiantes, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 380, -1, -1));

        jButton1.setBackground(new java.awt.Color(51, 153, 255));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/addUser.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 170, 120, -1));

        jButton2.setBackground(new java.awt.Color(51, 153, 240));
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/informationuser.png"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 170, 120, -1));

        jButton3.setBackground(new java.awt.Color(51, 153, 240));
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/impresora.png"))); // NOI18N
        jButton3.setText("jButton3");
        getContentPane().add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 170, -1, -1));

        label_wallpaper.setBackground(new java.awt.Color(153, 204, 255));
        label_wallpaper.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 204), 4));
        getContentPane().add(label_wallpaper, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 710, 429));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       
        RegistrarVisitas regvisitas = new RegistrarVisitas();
        regvisitas.setVisible(true); 
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        
        GestionarVisita gestion = new GestionarVisita();
        gestion.setVisible(true);
        
    }//GEN-LAST:event_jButton2ActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Visitas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Visitas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Visitas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Visitas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Visitas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel label_estudiantes;
    private javax.swing.JLabel label_gestionar_visita;
    private javax.swing.JLabel label_imprimir_visita;
    private javax.swing.JLabel label_nombre_usuario;
    private javax.swing.JLabel label_registro_visita;
    private javax.swing.JLabel label_wallpaper;
    // End of variables declaration//GEN-END:variables
}
