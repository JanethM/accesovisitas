package ventanas;

import java.awt.Image;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import static ventanas.Login.user;
import Coneccion.Coneccion;
import java.awt.Color;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class RegistrarVisitas extends javax.swing.JFrame {

    public RegistrarVisitas()
    {
        initComponents();     
        user = Login.user;
        setTitle("Registrar nueva Visita - Sesión de " + user);
        setSize(575, 472);
        setResizable(false);
        setLocationRelativeTo(null);
        System.out.println("prueba janeth");
          //----------Se pone un label wallpaper se le pone una imagen  y se saca de paquete con nombre y extencion 
        ImageIcon wallpaper = new ImageIcon("src/images/wallpaperPrincipal.jpg");

        //----------Se nombra al objeto Icon y se pone las mismas medidas del label para que quede al mismo tamaño
        Icon icono = new ImageIcon(wallpaper.getImage().getScaledInstance(label_wallpaper.getWidth(),
                label_wallpaper.getHeight(), Image.SCALE_DEFAULT));

        //----------Se establece la imgen escogida al label 
        label_wallpaper.setIcon(icono);

        //-----------Para que se actualice en cada modificacion       
        this.repaint();     
    }

 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        text_nombre = new javax.swing.JTextField();
        text_apellido = new javax.swing.JTextField();
        text_telefono = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        text_motivo = new javax.swing.JTextArea();
        cmb_area = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        btn_agregarVsitante = new javax.swing.JButton();
        label_wallpaper = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        text_nombre.setBackground(new java.awt.Color(153, 153, 255));
        getContentPane().add(text_nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 60, 130, 30));

        text_apellido.setBackground(new java.awt.Color(153, 153, 255));
        getContentPane().add(text_apellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 110, 130, 30));

        text_telefono.setBackground(new java.awt.Color(153, 153, 255));
        getContentPane().add(text_telefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 160, 130, 30));

        text_motivo.setBackground(new java.awt.Color(153, 153, 255));
        text_motivo.setColumns(20);
        text_motivo.setRows(5);
        jScrollPane1.setViewportView(text_motivo);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 270, 200, 120));

        cmb_area.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione", "Recursos Humanos", "Contabilidad", "Finanzas", "Desarrollo", "Sistemas", " " }));
        getContentPane().add(cmb_area, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 210, -1, 30));

        jLabel1.setFont(new java.awt.Font("Calisto MT", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 102));
        jLabel1.setText("NOMBRE:");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 70, 90, -1));

        jLabel2.setFont(new java.awt.Font("Calisto MT", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 102));
        jLabel2.setText("APELLIDO:");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 120, -1, -1));

        jLabel3.setFont(new java.awt.Font("Calisto MT", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 102));
        jLabel3.setText("TELEFONO:");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 170, -1, 20));

        jLabel4.setFont(new java.awt.Font("Calisto MT", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 0, 102));
        jLabel4.setText("Área que visita");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 210, 110, 30));

        jLabel5.setFont(new java.awt.Font("Calisto MT", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 102));
        jLabel5.setText("MOTIVO:");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 310, 80, -1));

        btn_agregarVsitante.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/addUser.png"))); // NOI18N
        btn_agregarVsitante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_agregarVsitanteActionPerformed(evt);
            }
        });
        getContentPane().add(btn_agregarVsitante, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 150, 140, 90));

        label_wallpaper.setBackground(new java.awt.Color(255, 255, 255));
        label_wallpaper.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 204), 4));
        getContentPane().add(label_wallpaper, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 560, 470));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_agregarVsitanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_agregarVsitanteActionPerformed
     
       String nombre, telefono, apellido, area, motivo;
        nombre = text_nombre.getText().trim();
        telefono = text_telefono.getText().trim();
        apellido = text_apellido.getText().trim();
        area = cmb_area.getItemAt(cmb_area.getSelectedIndex());
        motivo = text_motivo.getText().trim();

        if (verificDatos())
        {
             try {
                Connection cn = Coneccion.conectar();
                PreparedStatement pst = cn.prepareStatement(
                        //inserta dentro de la tabla visitas los valores que vas a recibir
                        "insert into visitantes values (?,?,?,?,?,?)");

                pst.setInt(1, 0);
                pst.setString(2, nombre);
                pst.setString(3, apellido);
                pst.setString(4, telefono);
                pst.setString(5, area);
                pst.setString(6, motivo);
                
                pst.executeUpdate();
                cn.close();
                limpiar();            
                   text_apellido.setBackground(Color.green);
                   text_nombre.setBackground(Color.green);
                   text_motivo.setBackground(Color.green);
                   text_telefono.setBackground(Color.green);
                   
                   JOptionPane.showMessageDialog(null, "Registro exitoso.");
              //este metodo this.dispose es para liberar recuross cuando le das acpetar en automatico da por teminado para que sigas registrando mas clientes y usuarioso
                   this.dispose();

            } 
             catch (SQLException e)
             {
                System.out.println("Error en registar visita " + e);
                JOptionPane.showMessageDialog(null, "Error al registar visita contacte al administardor");
             }     
    }//GEN-LAST:event_btn_agregarVsitanteActionPerformed
   }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegistrarVisitas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegistrarVisitas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegistrarVisitas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegistrarVisitas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RegistrarVisitas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_agregarVsitante;
    private javax.swing.JComboBox<String> cmb_area;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel label_wallpaper;
    private javax.swing.JTextField text_apellido;
    private javax.swing.JTextArea text_motivo;
    private javax.swing.JTextField text_nombre;
    private javax.swing.JTextField text_telefono;
    // End of variables declaration//GEN-END:variables

public boolean verificDatos()
{
    boolean resul = false;
    
    if(!text_nombre.getText().isEmpty())
    {
        if(!text_apellido.getText().isEmpty())
        {
            if(!text_telefono.getText().isEmpty())
            {
                if(cmb_area.getSelectedIndex()!= 0)
                {
                    if(!text_motivo.getText().isEmpty())
                    {
                        resul = true;            
                    }
                    else
                    {
                       JOptionPane.showMessageDialog(null, "Por favor ingresa el motivo por el cual visita"); 
                    }    
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "Por favor ingresa el area donde visita"); 
                }
            }
            else
            {
              JOptionPane.showMessageDialog(null, "Por favor ingresa el telefono del visitante");  
            }           
        }
        else
        {
           JOptionPane.showMessageDialog(null, "Por favor ingresa apellido del visitante");   
        }         
    }
    else
    {
           JOptionPane.showMessageDialog(null, "Por favor ingresa nombre del visitante");
    }   
    return resul;
}
    
  public void limpiar()
  {    
      text_apellido.setText("");
      text_nombre.setText("");
      text_motivo.setText("");
      text_telefono.setText("");   
      cmb_area.setSelectedItem(0);
  }
}
